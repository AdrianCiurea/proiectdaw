﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProiectDaW.Models;

namespace ProiectDaW.Controllers
{
    public class medicamenteController : Controller
    {
        private farmacieEntities db = new farmacieEntities();

        // GET: medicamente
        public async Task<ActionResult> Index()
        {
            var medicamentes = db.medicamentes.Include(m => m.producatori);
            return View(await medicamentes.ToListAsync());
        }

        // GET: medicamente/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medicamente medicamente = await db.medicamentes.FindAsync(id);
            if (medicamente == null)
            {
                return HttpNotFound();
            }
            return View(medicamente);
        }

        // GET: medicamente/Create
        public ActionResult Create()
        {
            ViewBag.id_producator = new SelectList(db.producatoris, "id", "denumire");
            return View();
        }

        // POST: medicamente/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,nume,cantitate,pret_bucata,id_producator")] medicamente medicamente)
        {
            if (ModelState.IsValid)
            {
                db.medicamentes.Add(medicamente);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.id_producator = new SelectList(db.producatoris, "id", "denumire", medicamente.id_producator);
            return View(medicamente);
        }

        // GET: medicamente/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medicamente medicamente = await db.medicamentes.FindAsync(id);
            if (medicamente == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_producator = new SelectList(db.producatoris, "id", "denumire", medicamente.id_producator);
            return View(medicamente);
        }

        // POST: medicamente/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,nume,cantitate,pret_bucata,id_producator")] medicamente medicamente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(medicamente).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.id_producator = new SelectList(db.producatoris, "id", "denumire", medicamente.id_producator);
            return View(medicamente);
        }

        // GET: medicamente/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medicamente medicamente = await db.medicamentes.FindAsync(id);
            if (medicamente == null)
            {
                return HttpNotFound();
            }
            return View(medicamente);
        }

        // POST: medicamente/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            medicamente medicamente = await db.medicamentes.FindAsync(id);
            db.medicamentes.Remove(medicamente);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
